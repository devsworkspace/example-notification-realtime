package com.alejandro.exmnotificationmedtrainer.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.alejandro.exmnotificationmedtrainer.R
import com.alejandro.exmnotificationmedtrainer.model.Product
import com.alejandro.exmnotificationmedtrainer.util.ProductItemAdapter
import com.alejandro.exmnotificationmedtrainer.viewmodel.ProductListViewModel
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.RemoteMessage
import kotlinx.android.synthetic.main.product_list_fragment.*

class ProductListFragment : Fragment() {

    private lateinit var viewModel: ProductListViewModel
    private val adapter = ProductItemAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.product_list_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        FirebaseMessaging.getInstance().subscribeToTopic("all")
        viewModel = ViewModelProviders.of(this)
            .get(ProductListViewModel::class.java)

        rvProducts.setHasFixedSize(true)
        rvProducts.layoutManager = LinearLayoutManager(context)
        rvProducts.itemAnimator = DefaultItemAnimator()
        rvProducts.adapter = adapter

        if (!viewModel.listProducts.hasObservers()) {
            viewModel.listProducts.observe(this, Observer<List<Product>> {
                adapter.setData(it)
                rvProducts.smoothScrollToPosition(it.size)
            })
        }
        btnAdd.setOnClickListener { findNavController(this).navigate(R.id.action_productListFragment_to_addProductFragment) }
    }


}
