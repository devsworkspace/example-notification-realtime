package com.alejandro.exmnotificationmedtrainer.view

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.SurfaceHolder
import android.view.View
import android.view.ViewGroup
import androidx.core.util.isNotEmpty
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.alejandro.exmnotificationmedtrainer.R
import com.alejandro.exmnotificationmedtrainer.databinding.AddProductFragmentBinding
import com.alejandro.exmnotificationmedtrainer.util.showToast
import com.alejandro.exmnotificationmedtrainer.viewmodel.AddProductViewModel
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import kotlinx.android.synthetic.main.add_product_fragment.*


/**
 * @author Alejandro Salazar ~ salazaralejandro767@gmail.com
 */
class AddProductFragment : Fragment() {

    private lateinit var viewModel: AddProductViewModel
    private lateinit var binding: AddProductFragmentBinding
    private lateinit var detector: BarcodeDetector
    private lateinit var cameraSource: CameraSource

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.add_product_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(AddProductViewModel::class.java)
        binding.vm = viewModel
        binding.lifecycleOwner = this

        if (!viewModel.status.hasObservers())
            viewModel.status.observe(this, Observer<Int> {
                when (it) {
                    AddProductViewModel.PRODUCT_SAVED -> {
                        NavHostFragment.findNavController(this).popBackStack()
                        showToast("Product saved !")
                    }
                    AddProductViewModel.PRODUCT_INVALID -> showToast("Insert a name")

                }
            })



        detector = BarcodeDetector.Builder(context)
            .setBarcodeFormats(Barcode.ALL_FORMATS)
            .build()
        cameraSource = CameraSource.Builder(context, detector)
            .setAutoFocusEnabled(true)
            .setRequestedPreviewSize(640, 900)
            .build()

        detector.setProcessor(object : Detector.Processor<Barcode> {
            override fun release() {
                println("release")
            }

            override fun receiveDetections(detections: Detector.Detections<Barcode>?) {
                val detectionItems = detections?.detectedItems
                if (detectionItems != null && detectionItems.isNotEmpty()) {
                    viewModel.setBarcode(detectionItems.valueAt(0).rawValue)
                }
            }
        })

        swCameraPreview.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), 0)
            }

            override fun surfaceDestroyed(holder: SurfaceHolder?) {
                cameraSource.stop()
            }

            override fun surfaceCreated(holder: SurfaceHolder?) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), 0)
            }
        })
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        for (permission in permissions) {
            if (permission == Manifest.permission.CAMERA && grantResults[permissions.indexOf(permission)] == PackageManager.PERMISSION_GRANTED)
                cameraSource.start(swCameraPreview.holder)
            else
                swCameraPreview.visibility = View.INVISIBLE
        }
    }
}
