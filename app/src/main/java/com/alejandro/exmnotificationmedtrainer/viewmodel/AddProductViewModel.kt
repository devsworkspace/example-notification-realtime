package com.alejandro.exmnotificationmedtrainer.viewmodel

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.alejandro.exmnotificationmedtrainer.model.Product
import com.alejandro.exmnotificationmedtrainer.repository.ProductRepository

class AddProductViewModel(application: Application) : AndroidViewModel(application) {

    companion object {
        const val PRODUCT_SAVED = 0
        const val PRODUCT_INVALID = 1
    }

    val name = ObservableField<String>()
    val codebar = ObservableField<String>()

    private val repository = ProductRepository(application)
    val status = MutableLiveData<Int>()


    fun setBarcode(barcode: String) {
        codebar.set(barcode)
    }

    fun saveProduct() {
        this.name.get()?.let {
            status.value = PRODUCT_INVALID
        }
        val product = Product()
        product.name = this.name.get() ?: ""
        product.codebar = this.codebar.get()
        repository.insertProduct(product)
        status.value = PRODUCT_SAVED
    }

}
