package com.alejandro.exmnotificationmedtrainer.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.alejandro.exmnotificationmedtrainer.repository.ProductRepository

class ProductListViewModel(application: Application) : AndroidViewModel(application) {

    val listProducts = ProductRepository(application).getProducts()

}
