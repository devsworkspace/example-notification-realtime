package com.alejandro.exmnotificationmedtrainer.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*
import kotlin.collections.HashMap


@Entity(tableName = Product.TABLE_NAME)
class Product {

    companion object {
        const val TABLE_NAME = "product"
    }

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "product_id")
    var productId: Int = 0

    @ColumnInfo(name = "name")
    var name: String = ""

    @ColumnInfo(name = "codebar")
    var codebar: String? = null

    fun isValidateProduct(): Boolean {
        return name.isNotEmpty()
    }

    fun toMap(): Map<String, Any?> {
        val productMap = HashMap<String, Any?>()
        productMap["name"] = name
        productMap["codebar"] = codebar
        return productMap
    }
}