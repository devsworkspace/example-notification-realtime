package com.alejandro.exmnotificationmedtrainer.model

import com.google.gson.annotations.SerializedName

data class Notification(
    var data: Product = Product(),
    val to: String = "/topics/all",
    @SerializedName("analytics_label")
    val analyticsLabel: String = "test"
)