package com.alejandro.exmnotificationmedtrainer.repository

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.alejandro.exmnotificationmedtrainer.model.Product
import com.alejandro.exmnotificationmedtrainer.util.Converters

@Database(entities = [Product::class], version = 1)
@TypeConverters(Converters::class)
abstract class ProductDatabase : RoomDatabase() {
    abstract fun productDao(): ProductDao

    companion object {
        private const val DATABASE_NAME = "pruducts_database"
        private var INSTANCE: ProductDatabase? = null

        fun getInstance(context: Context): ProductDatabase {
            if (INSTANCE == null)
                INSTANCE = Room.databaseBuilder(
                    context,
                    ProductDatabase::class.java,
                    DATABASE_NAME
                ).build()
            return INSTANCE!!
        }

    }

}