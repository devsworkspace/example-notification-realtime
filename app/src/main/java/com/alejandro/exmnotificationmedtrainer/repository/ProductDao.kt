package com.alejandro.exmnotificationmedtrainer.repository

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.alejandro.exmnotificationmedtrainer.model.Product

@Dao
interface ProductDao {
    @Insert
    fun insert(product: Product)

    @Query("SELECT * FROM ${Product.TABLE_NAME}")
    fun getProducts(): LiveData<List<Product>>?

    @Query("SELECT * FROM ${Product.TABLE_NAME} WHERE name = :name")
    fun findProduct(name: String): List<Product>?
}