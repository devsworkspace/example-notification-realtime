package com.alejandro.exmnotificationmedtrainer.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.alejandro.exmnotificationmedtrainer.model.Notification
import com.alejandro.exmnotificationmedtrainer.model.Product
import com.alejandro.exmnotificationmedtrainer.wsclient.ResourceGenerator
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ProductRepository(application: Application) {

    private val productDao = ProductDatabase
        .getInstance(application)
        .productDao()

    private val notificationRepository = ResourceGenerator.notificationResource

    fun getProducts(): LiveData<List<Product>> {
        return productDao.getProducts() ?: MutableLiveData()
    }

    fun insertProduct(product: Product?) {
        product?.let {
            notificationRepository.sendNotification(Notification(it))
                .enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        println("Error ${t.message}")
                    }

                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        println("CODE: ${response.code()}")
                        println("BODY: ${response.body()?.string()}")
                        println("BAD: ${response.errorBody()?.string()}")
                    }
                })
            InsertAsyncTask(productDao).execute(it)
        }

    }

    fun findProduct(name: String): List<Product> {
        return productDao.findProduct(name) ?: ArrayList()
    }


    private class InsertAsyncTask(private val productDao: ProductDao) : AsyncTask<Product, Void, Void>() {

        override fun doInBackground(vararg products: Product?): Void? {
            for (product in products) {
                product?.let {
                    productDao.insert(it)
                }
            }
            return null
        }
    }
}