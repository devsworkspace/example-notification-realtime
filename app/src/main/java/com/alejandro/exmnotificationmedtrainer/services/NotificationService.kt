package com.alejandro.exmnotificationmedtrainer.services

import com.alejandro.exmnotificationmedtrainer.model.Product
import com.alejandro.exmnotificationmedtrainer.repository.ProductRepository
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import org.json.JSONObject

class NotificationService : FirebaseMessagingService() {

    private val gson = GsonBuilder()
        .create()
    private lateinit var repository: ProductRepository

    override fun onCreate() {
        super.onCreate()
        repository = ProductRepository(application)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)
        println("Message: ${remoteMessage?.data}")

        remoteMessage?.let {
            try {
                val product = gson.fromJson(JSONObject(it.data).toString(), Product::class.java)
                if (repository.findProduct(product.name).isEmpty()) {
                    repository.insertProduct(product)
                }
            } catch (e: JsonSyntaxException) {
                println("E JsonSyntaxException ${e.message}")
            }
        }

    }
}
