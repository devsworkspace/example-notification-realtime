package com.alejandro.exmnotificationmedtrainer.wsclient

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ResourceGenerator {
    private const val URL_BASE = "https://fcm.googleapis.com/fcm/"

    val notificationResource = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .client(OkHttpClient.Builder().build())
        .baseUrl(URL_BASE)
        .build().create(INotificationResource::class.java)

}