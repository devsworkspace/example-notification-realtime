package com.alejandro.exmnotificationmedtrainer.wsclient


import com.alejandro.exmnotificationmedtrainer.model.Notification

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface INotificationResource {

    @POST("send")
    @Headers(
        "Content-Type: application/json",
        "authorization: key=AAAADLSSbzk:APA91bEeliwQawFFsN63qkVd0AcjXoAKG8Kevp29C4QTKkO31bxPAJBNXYhfv2lwKGJ5FasXpR5V4FoS2Eqe1T5jzUS1dqaY1zu2M7UYZmVJMfSIy5Z6ZyqBhW1HkGa1LzH1kcSQHQe1"
    )
    fun sendNotification(@Body notification: Notification): Call<ResponseBody>

}