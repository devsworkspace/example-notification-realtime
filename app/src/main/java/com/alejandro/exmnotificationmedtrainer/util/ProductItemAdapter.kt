package com.alejandro.exmnotificationmedtrainer.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alejandro.exmnotificationmedtrainer.R
import com.alejandro.exmnotificationmedtrainer.model.Product
import kotlinx.android.synthetic.main.product_item.view.*

class ProductItemAdapter :
    RecyclerView.Adapter<ProductItemAdapter.ProductViewHolder>() {

    private var products: List<Product>? = null

    fun setData(products: List<Product>) {
        this.products = products
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder =
        ProductViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent, false))

    override fun getItemCount(): Int {
        products?.let {
            return products!!.size
        }
        return 0
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) = holder.binding(products!![position])


    class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun binding(product: Product) {
            itemView.tvName.text = product.name
            itemView.tvBarcode.text = product.codebar
        }
    }
}