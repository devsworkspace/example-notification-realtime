package com.alejandro.exmnotificationmedtrainer.util

import androidx.room.TypeConverter
import java.util.*

class Converters {

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? = value?.let { Date(it) }

    @TypeConverter
    fun dateToTimestamp(value: Date?): Long? = value.let { it!!.time }

}